import java.util.*;
import com.google.gson.GsonBuilder;

public class IsildakChain {
    public static ArrayList<Block> blockchain=new ArrayList<>();
    public static int difficulty=2;
    public static void main(String[] args) {

        blockchain.add(new Block("Ilk Blok ", "0"));
        System.out.println("Ilk blok yapılıyor........");
        blockchain.get(0).minining(difficulty);

        blockchain.add(new Block("Ikinci Blok",blockchain.get(blockchain.size()-1).hash));
        System.out.println("Ikinci Blok yapılıyor......");
        blockchain.get(1).minining(difficulty);

        blockchain.add(new Block("Ucuncu Blok",blockchain.get(blockchain.size()-1).hash));
        System.out.println("Ucuncu Blok Yapılıyor......");
        blockchain.get(2).minining(difficulty);

        System.out.println("\nBlockchain is Valid: " + Block.isChainValid());

        String blockchainJson = new GsonBuilder().setPrettyPrinting().create().toJson(blockchain);
        System.out.println("\nThe block chain: ");
        System.out.println(blockchainJson);
    }
}
