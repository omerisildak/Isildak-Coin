import java.security.MessageDigest;

/*
Burada sha 256 kullımış  stringten hash elde ediyoruz bunuda hex haline getiriyoruz çünkü hex 0-F arasında olduğu için daha fazla depolama sağlar
 */
public class StringUtil {

    public static String Sha256(String input){
        try{
            MessageDigest digest=MessageDigest.getInstance("SHA-256");
            byte[] hash=digest.digest(input.getBytes("UTF-8"));
            StringBuffer hexString=new StringBuffer();
            for (int i = 0; i <hash.length ; i++) {
                String hex=Integer.toHexString(0xff & hash[i]);
                if (hex.length()==1){
                    hexString.append('0');
                }
                else {
                    hexString.append(hex);
                }
            }

            return hexString.toString();
        }
        catch (Exception e){
            throw new RuntimeException(e);
        }

    }
}
