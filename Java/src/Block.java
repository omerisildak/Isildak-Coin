import java.util.Date;

public class Block {

    public String hash;//its unique
    public String previousHash;
    private String data;//our data is a simple message
    private long timeStamp;//time
    private int forNow;
    public Block(String data,String previousHash){
        this.data=data;
        this.previousHash=previousHash;
        this.timeStamp=new Date().getTime();
        this.hash=calculateHash();
    }
    public String calculateHash(){
        String  calculatedHash=StringUtil.Sha256(previousHash+
                Long.toString(timeStamp)+
                data
                );
        return calculatedHash;
    }
    public static Boolean isChainValid() {//Valided i check ediyoruz eğer aynısında varsa false dönecek
        Block currentBlock;
        Block previousBlock;

        for(int i = 1; i < IsildakChain.blockchain.size(); i++) {//bir for dongüsü eğer eşit varsa fail eder.
            currentBlock = IsildakChain.blockchain.get(i);
            previousBlock = IsildakChain.blockchain.get(i-1);
            if(!currentBlock.hash.equals(currentBlock.calculateHash()) ){
                System.out.println("Current Hashes not equal");
                return false;
            }
            if(!previousBlock.hash.equals(currentBlock.previousHash) ) {
                System.out.println("Previous Hashes not equal");
                return false;
            }
        }
        return true;
    }
    /*
     mining fonksiyonnda int olarak sayı veriyoruz 0 minimum
      */
    public  void minining(int difficulty){
        String target=new String(new char[difficulty]).replace('\0','0');
        while (!hash.substring(0,difficulty).equals(target)){
            forNow++;
            hash=calculateHash();
        }
        System.out.println("My Mined Block : "+hash);

    }
}
